package solvers;

import data.Direction;

public interface Solver {
	Direction getHint(int[][] state, int score);
}
