package solvers;

import solvers.minimax.AIsolver;
import solvers.minimax.Board;
import data.Direction;

public class MinimaxSolver implements Solver {

	@Override
	public Direction getHint(int[][] state, int score) {
		int depth = 7;
		Board board = new Board(state, score);
		solvers.minimax.Direction direction = null;
		try {
			direction = AIsolver.findBestMove(board, depth);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		Direction result = convertDirection(direction);
		System.out.println("Sending help: " + result);
		return result;
	}

	private Direction convertDirection(solvers.minimax.Direction direction) {
		if (direction == null)
			return Direction.NONE;
		switch (direction) {
			case DOWN:
				return Direction.DOWN;
			case LEFT:
				return Direction.LEFT;
			case RIGHT:
				return Direction.RIGHT;
			case UP:
				return Direction.UP;
		}
		return null;
	}
}
