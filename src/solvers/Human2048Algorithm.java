package solvers;

import data.Direction;

@Deprecated
public class Human2048Algorithm implements Solver {

	@Override
	public Direction getHint(int[][] state, int score) {
		return Direction.UP;
	}

	// @Override
	// public Direction[] solve(int[][] state, int hints) {
	// Direction[] directions = new Direction[hints];
	// Game2048Model model = new Game2048Model(state.length);
	// for (int i = 0; i < directions.length; i++) {
	// int[][] newState = cloneState(state);
	// model.setData(newState);
	// Direction[] possibleDirection = getNextMove(i);
	// for (int d = 0; d < possibleDirection.length; d++) {
	// applyDirection(model, possibleDirection[d]);
	// if (!areEqual(state, model.getData())) {
	// directions[i] = possibleDirection[d];
	// state = newState;
	// break;
	// }
	// }
	// }
	//
	// return directions;
	// }
	//
	// private int[][] cloneState(int[][] state) {
	// int[][] clone = new int[state.length][];
	// for (int i = 0; i < state.length; i++)
	// clone[i] = state[i].clone();
	//
	// return clone;
	// }
	//
	// private Direction[] getNextMove(int i) {
	// Direction[] directions = new Direction[4];
	//
	// if (i % 2 == 0) {
	// directions[0] = Direction.LEFT;
	// directions[1] = Direction.DOWN;
	// } else {
	// directions[0] = Direction.DOWN;
	// directions[1] = Direction.LEFT;
	// }
	// directions[2] = Direction.RIGHT;
	// directions[3] = Direction.UP;
	//
	// return directions;
	// }
	//
	// private void applyDirection(Model model, Direction direction) {
	// switch (direction) {
	// case DOWN:
	// model.moveDown();
	// break;
	// case LEFT:
	// model.moveLeft();
	// break;
	// case RIGHT:
	// model.moveRight();
	// break;
	// case UP:
	// model.moveUp();
	// break;
	// }
	// }
	//
	// private boolean areEqual(int[][] state1, int[][] state2) {
	// for (int i = 0; i < state1.length; i++) {
	// for (int j = 0; j < state1.length; j++) {
	// if (state1[i][j] != state2[i][j]) {
	// return false;
	// }
	// }
	// }
	//
	// return true;
	// }
}
