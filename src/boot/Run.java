package boot;

import org.eclipse.swt.widgets.Display;

import server.ServerUI;

public class Run {
	public static void main(String[] args) {
		Display display = Display.getDefault();
		new ServerUI(display);
		while ((Display.getCurrent().getShells().length != 0)
				&& !Display.getCurrent().getShells()[0].isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
		System.exit(1);
	}
}
