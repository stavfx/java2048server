package server.handlers;

import server.Server;
import solvers.MinimaxSolver;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Direction;
import data.Game2048HelpRequest;

public class Game2048Handler extends AbstractHandler {
	public Game2048Handler(Server server) {
		super(server);
	}

	@Override
	protected void handle(String requestStr, HttpExchange exchange) {
		Gson gson = new Gson();
		Game2048HelpRequest request = gson.fromJson(requestStr, Game2048HelpRequest.class);

		Direction direction = new MinimaxSolver().getHint(request.getState(), request.getScore());

		respond(gson.toJson(direction), exchange);
	}

}
