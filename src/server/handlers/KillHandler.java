package server.handlers;

import server.Server;

import com.sun.net.httpserver.HttpExchange;

public class KillHandler extends AbstractHandler {

	public KillHandler(Server server) {
		super(server);
	}

	@Override
	protected void handle(String request, HttpExchange exchange) {
		System.out.println("kill time");
		respond("Bye bye!", exchange);
		server.stop();
		System.exit(0);
	}
}
