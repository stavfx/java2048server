package server.handlers;

import server.Server;

import com.sun.net.httpserver.HttpExchange;

public class BaseHandler extends AbstractHandler {

	public BaseHandler(Server server) {
		super(server);
	}

	@Override
	protected void handle(String request, HttpExchange exchange) {
		respond("Unsupported Operation!", exchange);
	}
}
