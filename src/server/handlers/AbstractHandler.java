package server.handlers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import server.Client;
import server.Server;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class AbstractHandler implements HttpHandler {
	// private HttpExchange exchange;
	protected Server server;

	public AbstractHandler(Server server) {
		this.server = server;
	}

	@Override
	public final void handle(HttpExchange exchange) throws IOException {
		String clientName = exchange.getRemoteAddress().getHostName();
		String clientMethod = exchange.getRequestURI().getPath();
		Client client = new Client(clientName, clientMethod);
		server.clientConnected(client);

		handle(getRequestBody(exchange), exchange);
		server.clientDisconnected(client);
	}

	protected abstract void handle(String request, HttpExchange exchange);

	private String getRequestBody(HttpExchange exchange) {
		try {
			InputStream is = exchange.getRequestBody();
			if (is != null) {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int count;
				while ((count = is.read(buffer)) != -1) {
					bos.write(buffer, 0, count);
				}
				return bos.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void respond(String response, HttpExchange exchange) {
		try {
			Headers responseHeaders = exchange.getResponseHeaders();
			responseHeaders.set("Content-Type", "application/json");
			exchange.sendResponseHeaders(200, 0);

			OutputStream out = exchange.getResponseBody();
			out.write(response.getBytes());
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected String[] getRequestParams(HttpExchange exchange) {
		String[] params = exchange.getRequestURI().getPath().split("/");
		if (params.length <= 2)
			return null;
		params = Arrays.copyOfRange(params, 2, params.length);
		return params;
	}
}