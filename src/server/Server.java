package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executors;

import server.handlers.BaseHandler;
import server.handlers.Game2048Handler;
import server.handlers.KillHandler;

import com.sun.net.httpserver.HttpServer;

public class Server implements Observer {
	private static final int PORT = 80;

	private HttpServer server;
	private ServerStateListener stateListener;
	private ArrayList<Client> clients = new ArrayList<Client>();

	public void start() {
		killLastServer();
		startListening();
	}

	public void stop() {
		if (server != null) {
			server.stop(1);
			if (stateListener != null)
				stateListener.onServerStopped();
		}
	}

	public void setServerStateListener(ServerStateListener listener) {
		this.stateListener = listener;
	}

	private void killLastServer() {
		try {
			URL url = new URL("http://localhost:" + PORT + "/kill");
			URLConnection connection = url.openConnection();
			connection.getInputStream();
		} catch (Exception e) {
			System.out.println("No pervious server found...");
		}
	}

	private void startListening() {
		InetSocketAddress addr = new InetSocketAddress(PORT);
		try {
			server = HttpServer.create(addr, 0);
		} catch (IOException e) {
			System.out.println("Failed to create server :(");
			System.out.println("...Port taken?");
			return;
		}

		initHandlers(server);

		server.setExecutor(Executors.newCachedThreadPool());
		server.start();
		System.out.println("Server is listening on port " + PORT);

		if (stateListener != null)
			stateListener.onServerStarted(PORT);
	}

	private void initHandlers(HttpServer server) {
		server.createContext("/", new BaseHandler(this));
		server.createContext("/2048", new Game2048Handler(this));
		server.createContext("/kill", new KillHandler(this));
	}

	public void clientConnected(Client client) {
		client.addObserver(this);
		clients.add(client);
		if (stateListener != null)
			stateListener.onClientListUpdated(clients);
	}

	public void clientDisconnected(Client client) {
		client.deleteObserver(this);
		clients.remove(client);
		if (stateListener != null)
			stateListener.onClientListUpdated(clients);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (stateListener != null)
			stateListener.onClientListUpdated(clients);
	}
}
