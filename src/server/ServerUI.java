package server;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class ServerUI implements ServerStateListener, SelectionListener {
	private Display display;
	private Shell shell;

	private Button btnStartServer;
	private Button btnStopServer;
	// private Button btnKillClient;

	private Server server;
	private Label lblStatus;
	private List listClients;

	public ServerUI(Display display) {
		this.display = display;
		server = new Server();
		server.setServerStateListener(this);

		shell = new Shell(display);
		shell.setSize(700, 400);
		shell.setLayout(new GridLayout(2, false));
		shell.setText("Server Console");

		btnStartServer = new Button(shell, SWT.PUSH);
		btnStartServer.setText("Start");
		btnStartServer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		btnStartServer.addSelectionListener(this);

		btnStopServer = new Button(shell, SWT.PUSH);
		btnStopServer.setText("Stop");
		btnStopServer.setEnabled(false);
		btnStopServer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		btnStopServer.addSelectionListener(this);

		// new Label(shell, SWT.NONE);// Spacer

		Label label = new Label(shell, SWT.NONE);
		label.setText("Connected Clients:");
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1));

		listClients = new List(shell, SWT.BORDER);
		listClients.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 2));

		// btnKillClient = new Button(shell, SWT.PUSH);
		// btnKillClient.setText("Kill Client");
		// btnKillClient.addSelectionListener(this);
		// new Label(shell, SWT.NONE);// Spacer

		lblStatus = new Label(shell, SWT.BORDER);
		lblStatus.setText("Server stopped...");
		lblStatus.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 3, 1));

		shell.open();
	}

	@Override
	public void onServerStarted(final int port) {
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				btnStartServer.setEnabled(false);
				btnStopServer.setEnabled(true);
				lblStatus.setText("Server running on port " + port);
			}
		});
	}

	@Override
	public void onServerStopped() {
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				btnStartServer.setEnabled(true);
				btnStopServer.setEnabled(false);
				lblStatus.setText("Server stopped...");
				listClients.removeAll();
			}
		});
	}

	@Override
	public void onClientListUpdated(final java.util.List<Client> clients) {
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				listClients.setItems(clientsToString(clients));
				listClients.update();
			}
		});
	}

	private String[] clientsToString(java.util.List<Client> clients) {
		String[] strings = new String[clients.size()];
		for (int i = 0; i < strings.length; i++) {
			strings[i] = clients.get(i).toString();
		}
		return strings;
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		if (e.widget == btnStartServer) {
			server.start();
		} else if (e.widget == btnStopServer) {
			server.stop();
		}
		// else if (e.widget == btnKillClient) {
		// }
	}
}
