package server;

import java.util.Observable;

public class Client extends Observable {
	private String hostName;
	private String method;

	public Client(String hostName, String method) {
		this.hostName = hostName;
		this.method = method;
	}

	@Override
	public String toString() {
		return String.format("%s - %s", hostName, method);
	}
}
