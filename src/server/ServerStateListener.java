package server;

import java.util.List;

public interface ServerStateListener {
	void onServerStarted(int port);

	void onServerStopped();

	void onClientListUpdated(List<Client> clients);
}
